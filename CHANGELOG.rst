Changelog
=========


Unreleased
----------


0.1.0 (2024-07-23)
------------------

* Add support for Django 4.0
* Replace usage of ``ugettext_lazy``


FIXME
-----
